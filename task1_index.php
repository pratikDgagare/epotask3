<!DOCTYPE html>

<?php 

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "task1";
$name = $nameErr = $email = $emailErr = $gender = $updates = "";

$conn = mysqli_connect($servername, $username, $password, $dbname);

if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
} 

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $nameErr = "Name is required";
  } 
  else {
    $name = $_POST["name"];
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $nameErr = "Only letters and white space allowed"; 
    }
  }
  
  if (empty($_POST["email"])) {
    $emailErr = "Email is required";
  } else {
    $email = $_POST["email"];
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Invalid email format"; 
    }
  }

  $gender = $_POST["gender"];
  $updates = $_POST["updates"];


  if(isset($_POST["submit"])){
    $query="INSERT INTO profile VALUES ('$name','$email','$gender','','')";
    if ($conn->query($query) === TRUE) {
      echo "New record created successfully<br>";
    }
    else {
      echo "Error: " . $query . "<br>" . $conn->error . '<br>';
    }
    for($i=0;$i<sizeof($updates)-1;$i++){
      $query="UPDATE profile SET $updates[$i]=1 WHERE name='$name'";  
      $conn->query($query);
    }
  }

}

?>

<html>
<head><title>Task1</title></head>
<body>

<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
  Enter your name:
  <input name="name" type="text" required>
  <span class="error"><?php echo $nameErr;?></span>
  <br><br>
  Enter your email:
  <input name="email" type="text" required>
  <span class="error"><?php echo $emailErr;?></span>
  <br><br>
  Select gender:
  <select name="gender">
    <option value="male">Male</option>
    <option value="female">Female</option>
    <option value="other">Other</option>
  </select>
  <br><br>
  Receive updates via:
  <input type="checkbox" name="updates[]" value="usms">SMS
  <input type="checkbox" name="updates[]" value="uemail">email
  <input type="checkbox" name="updates[]" value="null" hidden checked>
  <br><br>
  <input type="submit" name="submit" value="submit">
</form>

<?php
if(isset($_POST["submit"])){
  echo "<h2>Your Input:</h2>";
  echo 'Name: '.$name;
  echo "<br>";
  echo 'Email: '.$email;
  echo "<br>";
  echo 'Gender: '.$gender;
  echo "<br>";
  echo 'Updates:';
  for($i=0;$i<sizeof($updates)-1;$i++){
    echo ' '.$updates[$i];
  }
}
?>

</body>
</html>

