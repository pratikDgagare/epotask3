<!DOCTYPE html>

<?php 

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "task2";
$organisation = $enggstream = $appmode = $check = "";
$cref = array();

$conn = mysqli_connect($servername, $username, $password, $dbname);

if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
} 

if($_SERVER["REQUEST_METHOD"] == "POST"){

	$organisation = $_POST["organisation"];
	$enggstream = $_POST["enggstream"];
	$appmode = $_POST["appmode"];
	$check = $_POST["check"];

	$query = "SELECT location , organisation , enggstream , appmode , shared_by ";
	
	$conditions1 = array();
	for($i=0;$i<sizeof($check)-1;$i++){
		$conditions1[] = "$check[$i]";
		$cref[$check[$i]] = $check[$i];
	}

	if (count($conditions1) > 0) {
		$query .= ', ';
		$query .= implode(' , ', $conditions1);
	}

	if($organisation != "" || $appmode != "" || $enggstream != "")
	{

	    $conditions2 = array();

	    if(! empty($organisation)) {
	      $conditions2[] = "organisation='$organisation'";
	    }
	    if(! empty($enggstream)) {
	      $conditions2[] = "enggstream='$enggstream'";
	    }
	    if(! empty($appmode)) {
	      $conditions2[] = "appmode='$appmode'";
	    }

	    if (count($conditions2) > 0) {
	      $query .= " FROM data WHERE " . implode(' AND ', $conditions2);
	    }
    }
	else{
		$query .= " FROM data";
	}
	$result = mysqli_query($conn,$query);
}
?>

<html>

<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css.css">
  <script type="text/javascript">
    function selectAll() {
        var items = document.getElementsByName('check[]');
        for (var i = 0; i < items.length; i++) {
            if (items[i].type == 'checkbox')
                items[i].checked = true;
        }
    }

    function UnSelectAll() {
        var items = document.getElementsByName('check[]');
        for (var i = 0; i < items.length-1; i++) {
            if (items[i].type == 'checkbox')
                items[i].checked = false;
        }
    }			
</script>
</head>

<body>

<div>
	<h1 class="bar">Exam Preparation Online</h1>
</div>

<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
  <div class="col-sm-3">

  	<div class="back">
		<button type="submit" name="af" class="b">Apply filter</button><br>
		<h4>Organisation:</h4>
		<select class="dd" name="organisation">
			<option value="" <?php if($organisation=="") echo 'selected';?> >Select (Any)</option>
			<option value="l_t" <?php if($organisation=="l_t") echo 'selected';?> >L&T</option>
	    	<option value="wipro" <?php if($organisation=="wipro") echo 'selected';?> >WIPRO</option>
	    	<option value="tcs" <?php if($organisation=="tcs") echo 'selected';?> >TCS</option>
	    	<option value="Reliance IL" <?php if($organisation=="Reliance IL") echo 'selected';?> >Reliance IL</option>
	    	<option value="BMW" <?php if($organisation=="BMW") echo 'selected';?> >BMW</option>
	  	</select>
	  	<br><br>
	  	<h4>Engineering Stream:</h4>
	  	<select class="dd" name="enggstream">
	  		<option value="" <?php if($enggstream=="") echo 'selected';?> >Select (Any)</option>
	    	<option value="it" <?php if($enggstream=="it") echo 'selected';?> >IT</option>
	    	<option value="civil" <?php if($enggstream=="civil") echo 'selected';?> >Civil</option>
	    	<option value="Mechanical" <?php if($enggstream=="Mechanical") echo 'selected';?> >Mechanical</option>
	  	</select>
	  	<br><br>
	  	<h4>Application Mode:</h4>
	  	<select class="dd" name="appmode">
	  		<option value=""  <?php if($appmode=="") echo 'selected';?> >Select (Any)</option>
	    	<option value="On Campus" <?php if($appmode=="On Campus") echo 'selected';?> >On Campus</option>
	    	<option value="Off Campus"  <?php if($appmode=="Off Campus") echo 'selected';?> >Off Campus</option>
	  	</select>
	  	<br><br>
	</div>
	<div class="back"> 
	  	<button type="button" name="selectall" onclick="selectAll()" class="b">Select all</button>
	  	<span style="float: right">|</span>
	  	<button type="button" name="clearall" onclick="UnSelectAll()" class="b">Clear all</button><br>
	  	<label class="container">Selction Procedure
		  	<input type="checkbox" name="check[]" value="select_proc" hidden <?php if(array_key_exists("select_proc",$cref)) echo 'checked';?>>
		  	<span class="checkmark"></span>
	  	</label><br>
	  	<label class="container">Technical Interview
		  	<input type="checkbox" name="check[]" value="tech_invw" hidden <?php if(array_key_exists("tech_invw",$cref)) echo 'checked';?>>
		  	<span class="checkmark"></span>
	 	</label><br>	  	
	  	<label class="container">Analytical Questions
		  	<input type="checkbox" name="check[]" value="anal_q" hidden <?php if(array_key_exists("anal_q",$cref)) echo 'checked';?>>
		  	<span class="checkmark"></span>
	  	</label><br>
	  	<label class="container">HR Questions
		  	<input type="checkbox" name="check[]" value="hr_q" hidden <?php if(array_key_exists("hr_q",$cref)) echo 'checked';?>>
		  	<span class="checkmark"></span>
	  	</label><br>
	  	<label class="container">Suggestions
		  	<input type="checkbox" name="check[]" value="sugg" hidden <?php if(array_key_exists("sugg",$cref)) echo 'checked';?>>
		  	<span class="checkmark"></span>
	  	</label><br>
	  	<input type="checkbox" name="check[]" value="null" hidden checked>
    </div>

  </div>
</form>
 
<?php

 echo '<div class="col-sm-9 vr"><h1>INTERVIEW EXPERIENCES :: ';

 if(isset($_POST["af"])){
 	if(!empty($organisation)){
 		echo $organisation.'</h1><br><br>';}
	else if(!empty($enggstream)){
 		echo $enggstream.'</h1><br><br>';}
 	else if(!empty($appmode)){
 		echo $appmode.'</h1><br><br>';}
 	else echo 'All</h1><br><br>';	

 	if ($result->num_rows > 0) {
	    while($row = $result->fetch_assoc()) {
	    	echo '<div class="result">';
	        echo '<left>Job Location :</left><right>' . $row["location"]. '</right><br>';
	        echo '<left>Organisation :</left><right>' . $row["organisation"]. '</right><br>';
	        echo '<left>Engineering Stream :</left><right>' . $row["enggstream"]. '</right><br>';
	        echo '<left>Application Mode :</left><right>' . $row["appmode"]. '</right><br>';
	        if(array_key_exists("select_proc",$row)) echo '<left>Selection Procedure :</left><right>' . $row["select_proc"]. '</right><br>';
	        if(array_key_exists("tech_invw",$row)) echo '<left>Technical Interview :</left><right>' . $row["tech_invw"]. '</right><br>';
	        if(array_key_exists("anal_q",$row)) echo '<left>Analytical Questions :</left><right>' . $row["anal_q"]. '</right><br>';
	        if(array_key_exists("hr_q",$row)) echo '<left>HR Questions :</left><right>' . $row["hr_q"]. '</right><br>';
	        if(array_key_exists("sugg",$row)) echo '<left>Suggestions :</left><right>' . $row["sugg"]. '</right><br>';
	        echo '<left>Shared By :</eft><right>' . $row["shared_by"]. '</right><br>';
	        echo '</div><br><br>';
	    }
	}
	else{
 		echo '<h3>No Result</h3>';
	}
  } 
?>

</body>
</html>
